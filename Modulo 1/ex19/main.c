#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main () {
	pid_t p;
	char ficheiro[] = "";
	int status;
	p = fork();
	if(p==0) {
			int erro = execlp("mkdir","mkdir","backup",NULL);
			printf("o directório não foi criado \n");
			exit(erro);
		}
	while(strcmp(ficheiro,"sair") != 0) {
		printf("Introduza o nome de ficheiro que pretende fazer backup\n");
		scanf("%s",ficheiro);
	if(strcmp(ficheiro,"sair") == 0) {
			break;
			}	
		else{
			p = fork();
			if(p==0) {
				int erro = execlp("cp","cp",ficheiro,"./backup",NULL);
				printf("\nO ficheiro não foi copiado \n");
				exit(erro);
			}
	}
	}
	return 0;
	}
