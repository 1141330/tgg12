#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>

int main (){

	pid_t pids;
	int resultado[100000];
	int dados[100000];
	int z,i;
	for (z=0;z<100000;z++){
		dados[z]=z;
	}
	pids=fork();
	if(pids>0){
	for(i = 0;i < 50000; i++){
		resultado[i]=dados[i]*4+20;
		printf("Resultado Pai %d \n",resultado[i]);
	}
	}
	if(pids==0){
		for(i = 5000;i < 9999; i++){
		resultado[i]=dados[i]*4+20;
		printf("Resultado Filho %d \n",resultado[i]);
	}
	}
	return 0;

}
