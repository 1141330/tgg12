#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>


 int main ( void ){
	pid_t p[6], x;
	int status,i;
	
	/*Criação de processos
	sendo p[0]- pid do 1º filho , p[1]-pid do 2º filho , p[2]- pid do Pai*/
	for(i=0;i<6;i++){
		p[i]=fork();
		if(p[i]==0)
			break;
	}
	if(i==6){
		printf("[%d]:Eu sou o PAI!\n",getpid());//getpid() -retorna o pid do próprio
		for(i=0;i< 6;i++){
			//x=waitpid(p[i],&status,0); -bloqueia até que o filho indicado (p[i]) termine
			x=waitpid(-1,&status,0); //-1 -> comporta-se como a funcao wait(int *status);
			
			//Verifica se o filho terminou normalmente
			if(WIFEXITED(status)){
				printf("[%d]:Filho %d terminou com valor de saída %d\n", getpid(),x, WEXITSTATUS(status));
			}
		 }		
	}else{
			if(i==0){
				printf("[%d]:Eu sou o 1 FILHO! escrevo de 1...20000\n", getpid());
				exit(1);
			}else if (i==1){
				printf("[%d]:Eu sou o 2 FILHO!\n escrevo 20001 ate 400000 \n", getpid());
				exit(2);
			}else if(i==2){
				printf("[%d]:Eu sou o 3 FILHO!\n escrevo 20001 ate 400000 \n", getpid());
				exit(3);
			}else if(i==3){
				printf("[%d]:Eu sou o 4 FILHO!\n escrevo 600001 ate 800000 \n", getpid());
				exit(4);
			}else if(i==4){
				printf("[%d]:Eu sou o 5 FILHO!\n escrevo 800001 ate 1000000 \n", getpid());
				exit(5);
			}else if(i==5){
				printf("[%d]:Eu sou o 6 FILHO!\n escrevo 1000001 ate 1200000 \n", getpid());
				exit(6);
			}		
	}
 return 0;
}
