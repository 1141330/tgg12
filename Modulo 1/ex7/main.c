﻿# include <stdio.h>

# include <sys/types.h>

# include <unistd.h>



void M(char *a)
{
	//65='A' em ascii
	if(*a==65)
	{
		printf("Pai:%c  || getpid:%d getppid:%d\n",*a,getpid(),getppid());
	}
	//66='B' em ascii 
	if(*a==66)
	{
		printf("Filh:%c || getpid:%d getppid:%d\n",*a,getpid(),getppid());
	}

}

int main(){
	int i;
	pid_t p;
	char a='A';
	char b='B';
	
	printf("Processo Inicial:%d\n\n",getpid());
		for(i=0;i<2;i++){
			p=fork();
			//validação
			if(p<0){
				perror("Erro ao criar novo processo");
				exit(-1);
			}
			//Filho		
			if(p==0)
			{
				M(&b);
				if(i==1)//2ª ITERAÇÃO
				{
					M(&a);
					exit(0);
				}
				
				
				
			}
			//Pai		
				M(&a);
				sleep(2);
				
		}
sleep(2);
return 0;
}
