#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>

char cria_gemeos(pid_t lista[2]);
void babyDead(pid_t v[], int n);


int main()
{

	char ret;
	int numero_processos=6;
	pid_t pid[numero_processos];
	int i;
	int z=numero_processos/2;
	for(i=0;i<z;i++) 
	{
		ret = cria_gemeos(pid+(i*2)); // Vamos andar de 2 em 2 bytes.
		if(ret == 'a' || ret == 'b') exit(ret); 
	}
	
	babyDead(pid,numero_processos);
	printf("[%d] Pai vai terminar\n",getpid());
		
	
	
	return 0;
}
void babyDead(pid_t v[], int n)
{
	int x;
	int i;
	
		for(i=0;i<n;i++)
		{
			waitpid(v[i],&x,0);
			if (WIFEXITED(x)){			
				printf(" %dº Filho com pid [%d] terminou com valor de saída %c\n",i+1, v[i], WEXITSTATUS(x) );
			}
		}

}
//Vai receber o pid[2] na posiçao correcta para fazer apenas 2 iterações
char cria_gemeos(pid_t lista[2])
{
	int i;
	for(i=0;i<2;i++)
	{
		lista[i]=fork();
			if(lista[i]<0) perror("Ocorreu um erro ao fazer o fork\n");
			if(lista[i]==0)//Filho 1 e 2
			{
				printf ("eu sou o filho, com pid[%d] do pai com pid[%d]\n", getpid(), getppid());
				return i+'a'; //se i=0 ret:'a' , se i=1 ret:'b'
			}
	}
	return 'p'; //Pai
}
