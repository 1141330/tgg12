#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>


 int main ( void ){
	pid_t p[2], x;
	int status,i;
	
	/*Criação de processos
	sendo p[0]- pid do 1º filho , p[1]-pid do 2º filho , p[2]- pid do Pai*/
	for(i=0;i<2;i++){
		p[i]=fork();
		if(p[i]==0)
			break;
	}
	if(i==2){
		printf("[%d]:Eu sou o PAI!\n",getpid());//getpid() -retorna o pid do próprio
		for(i=0;i< 2;i++){
			//x=waitpid(p[i],&status,0); -bloqueia até que o filho indicado (p[i]) termine
			x=waitpid(-1,&status,0); //-1 -> comporta-se como a funcao wait(int *status);
			
			//Verifica se o filho terminou normalmente
			if(WIFEXITED(status)){
				printf("[%d]:Filho %d terminou com valor de saída %d\n", getpid(),x, WEXITSTATUS(status));
			}
		 }		
	}else{
			if(i==0){
				printf("[%d]:Eu sou o 1 FILHO!\n", getpid());
				sleep(5);
				exit(1);
			}else{
				printf("[%d]:Eu sou o 2 FILHO!\n", getpid());
				sleep(2);
				exit(2);
			}
	}
 return 0;
}
