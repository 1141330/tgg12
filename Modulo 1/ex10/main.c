#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>

int main (){
	const int tamp = 5;
	pid_t pids[tamp];
	int t,i,x=0,y=0,num = 1,estado;
	int tam = 100000,vec[tam];
	
	for(i = 0;i < tam; i++){
		vec[i] = i;
	}
	
	for( i = 0; i < tamp; i++){
		pids[i] = fork();
		
		if(pids[i] == -1){
			perror("fork failed\n");
			exit(-1);
		}	
		y = x+20000;
		
		if(pids[i] == 0){
			for(t = x; t < y ; t++){
				
				if(vec[t] == num){
					printf(" o numero %d encontra se na posiçao %d do vector\n",num,t-1);
					exit (i+1);
				}
			}
			exit (0);
			
		}
		x = x+20000;
	}
	printf("Pai a espera dos filhos\n");
	for(i = 0; i < tamp; i++){
		waitpid(pids[i],&estado,0);
		
		if (WIFEXITED(estado)){
		
			if(WEXITSTATUS(estado) != 0){
				printf("Pai: o filho %d  encontrou o valor\n",WEXITSTATUS(estado));
			}
		}
	}

	return 0;

}
