#include <stdio.h>
#include <stdlib.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h> 
int babyMaker(pid_t*, int);
void babyDead(pid_t*, int);
#define NFILHOS 3

int main (void){
pid_t pid [10];
int f;
//d)Alteração do "f<3" para "f<2"
for ( f =0; f <2; f ++){
	int i=0;
	i++;
	pid[i] = fork ();
	//e)
	if ( pid[0] < 0){
		printf (" Erro!!!!! \ n" );
	}
	if ( pid[0] > 0){
	printf (" Eu sou o PAI \ n" );
	}else{
		sleep (1);
	}
}
//e) f) g) c)
	pid_t proc[NFILHOS];
	
	int id = babyMaker(proc, NFILHOS);
	
	if (id == 0){
		
		babyDead(proc, NFILHOS); 
		
	}else{
		
		sleep(1);		
		exit(id);
	}

	return 0;
}

int babyMaker(pid_t v[], int n){

	int i;
	
	for (i = 0; i < n; i++){
		v[i] = fork();
		if (v[i] == -1){
			printf("Erro ao criar processo\n");
			exit(1);
		}
		if (v[i] == 0){
			return i+1;
		}
		if (v[i]>0){
			printf("Eu sou o pai e criei o %dº filho com pid [%d]\n",(i+1), v[i]);
		}
	}
	
	return 0;
}


void babyDead(pid_t v[], int n){

	int status;
	int i;
	
	for (i = 0; i < n; i++){
	
		waitpid(v[i], &status, 0);
		
		if (WIFEXITED(status)){
			printf ("o filho %d com pid = [%d] terminou\n", i+1, v[i]);
			WEXITSTATUS(status);
		}else{
			printf("o filho %d com pid = [%d] não terminou corretamente\n", i+1, v[i]);
		}
	}
	
}

