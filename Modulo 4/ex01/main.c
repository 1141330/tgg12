#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <time.h>
#include <semaphore.h>

int main(){
FILE *file;
pid_t pid[5];
sem_t *semaforo;
int i;

if((file = fopen("output.txt","w"))==NULL){//abre o ficheiro
	perror("Erro no ficheiro...");
	exit(1);
}

//semaforo inicializado a "1".
 if ((semaforo = sem_open("semaforo1", O_CREAT|O_EXCL, 0644, 1)) == SEM_FAILED) {
perror("No sem_open()");
exit(1);
}


for(i=0;i<5;i++){
	pid[i]=fork();


if(pid[i]==0){//Filho

	int n;
	sem_wait(semaforo);
		for(n=0;n<200;n++){
			fprintf(file,"%d",n);
		}
	sem_post(semaforo);	
	exit(0);
	
}else{
	wait(NULL);
	//execlp("more", "more", "output.txt", (char*)NULL); 
	int nr;
	nr=fgetc(file);
	while(nr != EOF){
		fscanf(file,"%d",&nr);
		printf("%d",nr);
	}
}
}
sem_unlink("semaforo1");
return 0;
}
