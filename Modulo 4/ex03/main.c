#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <semaphore.h>

int main(int argc, char *argv[]){
	pid_t pids;
	sem_t *semaforo;
if ((semaforo = sem_open("merda2", O_CREAT|O_EXCL, 0644, 1)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
}
	printf("ta aqui");
	pids=fork();
	if(pids==0){
		sem_wait(semaforo);
		FILE *fp=(FILE *) NULL;		
		fp=fopen(argv[1],"w");
		printf("Ta a entrar ");
		fprintf(fp,"Eu sou o processo com o PID %d",getpid());
		fclose(fp);
		sem_post(semaforo);
	}
	else //Filho
	{
	sem_wait(semaforo);
	FILE *fp=(FILE *) NULL;		
	fp=fopen(argv[1],"w");
	if (fp == (FILE *) NULL)
		{
        printf("Could not open command file: %s",argv[1]);
        exit(-1);
		}

	fprintf(fp,"Eu sou o processo com o PID %d",getpid());
	fclose(fp);
	sem_post(semaforo);
	}
	sem_unlink("merda2");
	return 0;
}
