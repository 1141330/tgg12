#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <time.h>
#include <semaphore.h>


int main(){

pid_t pid;
sem_t *semaforo;

//semaforo inicializado a "1".
if ((semaforo = sem_open("semaforo5", O_CREAT|O_EXCL, 0644, 1)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
}

pid=fork();
	
	if(pid>0){//pai
		int i;
		
		for(i=0;i<10;i++){
			sleep(2);
			sem_wait(semaforo);
			printf("Eu sou pai.\n");
			sem_post(semaforo);	
		}
		wait(NULL);
	}else{//filho
		int n;
		for(n=0;n<10;n++){
			sleep(2);
			sem_wait(semaforo);
			printf("Eu sou filho.\n");
			sem_post(semaforo);	
		}
		exit(0);
	}
	sem_unlink("semaforo5");
	return 0;
}
