#include "student.h"
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

int main (void) {
	int soma=0,i=0,media;
	int fd = shm_open("/mod3ex02", O_RDWR, 0);
	if(fd == -1) {
		perror("O escritor deve ser executado primeiro.\n");
		exit(-1);
	}
	
	student *s = (student*)mmap(NULL, sizeof(student), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	
	for( i=0; i<3;i++){
		soma=soma+s->num[i];
	}
	media=soma/3;
	printf("Media %d \n", soma);
	
	munmap(s, sizeof(student));
	close(fd);
	shm_unlink("/mod3ex02");
	
	return 0;
}
