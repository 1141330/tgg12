#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <time.h>
#define NR_DISC 10
#define STR_SIZE 50
typedef struct {
	int numero;
    char nome[STR_SIZE];
	int disciplinas[NR_DISC];
} aluno;

int main(int argc, char *argv[]) {
	
	int fd;
	int r,i;
	aluno *shared_dados;
	pid_t pid;
	
	
	/* criar o objeto de memÃ³ria partilhada */
    fd = shm_open("/main", O_CREAT | O_RDWR, S_IRWXU | S_IRWXG);
    if (fd < 0) {
        perror("No shm_open()");
        exit(1);
    }
    
    /* ajustar o tamanho da mem. partilhada */
    ftruncate(fd, sizeof(aluno));
    
    /* mapear a mem. partilhada */
    shared_dados = (aluno *)mmap(NULL, sizeof(aluno), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (shared_dados == NULL) {
        perror("No mmap()");
        exit(1);
    }
    
    printf("Insira o nome do aluno:\n");
    fgets(shared_dados->nome, 100, stdin);
    /*scanf("%s", &aluno.nome);*/
    printf("Insira o numero do aluno:\n");
    scanf("%d", &shared_dados->numero);
	for(i=0;i<NR_DISC;i++){
		printf("Insira o as notas das 10 disciplinas"); 
		scanf("%d",&shared_dados->disciplinas[i]);
	}
	 /* escrever na memoria partilhada 
    sprintf(shared_dados->nome, dados_aluno.nome);
    shared_dados->numero = dados_aluno.numero;*/
    int menor=20,maior=0,media=0,soma=0;
	pid = fork();
	if(pid==-1){
		perror("Fork falhou!");	
	}
	if(pid==0){
		/* ler a memoria partilhada */
		printf("Nome: %s", shared_dados->nome);
		printf("Numero: %d\n", shared_dados->numero);
		for(i=0;i<NR_DISC;i++){
		printf("Notas das disiciplinas %d\n",shared_dados->disciplinas[i]);
		soma+=shared_dados->disciplinas[i];	
		if(shared_dados->disciplinas[i]>maior){
			maior=shared_dados->disciplinas[i];
		}
		if(shared_dados->disciplinas[i]<menor){
			menor=shared_dados->disciplinas[i];
		}
		}
		media=soma/NR_DISC;
		printf("O aluno tem a media de %d com a menor nota %d e maior nota %d",media,menor,maior);
	} 
	wait(NULL);
	
	/* desfaz mapeamento */
    r=munmap(shared_dados, sizeof(aluno));
    if (r < 0) { /* verifica erro */
        perror("No munmap()");
        exit(1);
    }
    
    /* apaga a memoria partilhada do sistema */
    r=shm_unlink("/main");
    return 0;
}
