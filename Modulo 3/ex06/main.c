#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>


#define MAX_PATH_LENGTH 50
#define MAX_WORD_LENGTH 10
#define SHARED_TAG "/shared1"
#define NUM_FILHOS 10

typedef struct {
	char caminho[NUM_FILHOS][MAX_PATH_LENGTH];
	char palavra[NUM_FILHOS][MAX_WORD_LENGTH];
	int occorrencias[NUM_FILHOS];
}data;

int babyMaker(pid_t*, int);
void babyDead(pid_t*, int);

int main()
{
	data *apontador; //shared memory
	int data_size = sizeof(data);
	
	int fd;
    fd = shm_open(SHARED_TAG, O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
    ftruncate(fd, data_size);
    apontador = (data*) mmap(NULL, data_size , PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    
    int i;
	char * ficheiro[NUM_FILHOS]={"ficheiro1.txt","ficheiro2.txt","ficheiro3.txt","ficheiro4.txt","ficheiro5.txt","ficheiro6.txt","ficheiro7.txt","ficheiro8.txt","ficheiro9.txt","ficheiro10.txt"};
	
	for(i=0;i<NUM_FILHOS;i++)//Ficheiro
	{	
		printf("\nFicheiro %dº\n",i+1);
		strcpy(apontador->caminho[i],ficheiro[i]);
		printf("Inserir palavra a procurar:");
		scanf("%s",apontador->palavra[i]); //Inserir palavra
	}
	
	
	
	pid_t pid[NUM_FILHOS];
	int id=babyMaker(pid,NUM_FILHOS);
	
	if(id==0)//Pai
	{
		babyDead(pid,NUM_FILHOS);
		
		int i;
		for(i=0; i<NUM_FILHOS; i++)
		{
			printf("Número de ocorrencias do ficheiro %d: %d\n",i,apontador->occorrencias[i]);
		}
		munmap(apontador,data_size);
		close(fd);
		shm_unlink(SHARED_TAG);


	}
	else //Filho
	{
		FILE * ficheiro_abrir;
		ficheiro_abrir= fopen(apontador->caminho[id-1],"r"); //read mode
		if(ficheiro == NULL)
		{
			perror("Erro a abrir o ficheiro\n");
			exit(1);
		}
		
		//printf("%s\n", apontador->caminho[id-1]);
		char linha[500];
		int numero_de_ocorrencias=0;
		//palavra a palavra
		
		
		while(fgets(linha, sizeof(linha), ficheiro_abrir) != NULL) //linha a linha
		{	
			if(strstr(linha, apontador->palavra[id-1]) != NULL)
			{
				numero_de_ocorrencias++;
				
			}
		}
		
		apontador->occorrencias[id-1]=numero_de_ocorrencias; //Escreve
		fclose(ficheiro_abrir);
		close(fd);
		exit(0);
	}
	return 0;
}

int babyMaker(pid_t v[], int n){

	int i;
	
	for (i = 0; i < n; i++){
		v[i] = fork();
		if (v[i] == 0){
			return i+1;
		}
	}
	return 0;
}
void babyDead(pid_t v[], int n){

	int status;
	int i;
	
	for (i = 0; i < n; i++){
	
		waitpid(v[i], &status, 0);
		
		if (WIFEXITED(status)){
			//...WEXITSTATUS(status);
		}	
	}
}
