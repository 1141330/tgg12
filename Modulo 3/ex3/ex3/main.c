#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <time.h>

int main(){
	int integer = 100;

	pid_t pid;
	
	int fd = shm_open("/mod3ex03", O_CREAT | O_RDWR , S_IRUSR | S_IWUSR); //abre para leitura e escrita;
	if(fd == -1) {
		perror("Erro a criar a memoria partilhada.");
		exit(-1);
	}
	
	ftruncate(fd, sizeof(integer)); //alocar o tamanho da estrutura.
	
	int *s = (int*)mmap(NULL, sizeof(integer), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);//
	*(s)=integer;
	pid=fork();
		if(pid==0){//filho
			int def; 
			int j;
			for(j=0;j<1000;j++){
				int valor;
				valor=*(s);
				valor++;
				printf("o filho somou-lhe 1, logo = %d\n",valor);
				valor--;
				printf("o filho subtraiu-lhe 1, logo = %d\n",valor);
				*(s)=valor;
				sleep(1);
			}
			exit(0);
		}else{
	int status;
	int i;
	for(i=0;i<1000;i++){
		sleep(1);
		*(s)=*(s)+1;
		printf("o pai somou-lhe 1, logo = %d\n",*(s));
		*(s)=*(s)-1;
		printf("o pai subtraiu-lhe 1, logo = %d\n",*(s));
	}
	munmap(s,sizeof(integer));
	close(fd);
	shm_unlink("/mod3ex02");
}
return 0;

}
