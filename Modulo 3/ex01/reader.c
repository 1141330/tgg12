#include "student.h"
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

int main (void) {
	
	int fd = shm_open("/mod3ex01", O_RDWR, 0);
	if(fd == -1) {
		perror("O escritor deve ser executado primeiro.\n");
		exit(-1);
	}
	
	student *s = (student*)mmap(NULL, sizeof(student), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	
	printf("O estudante com numero %d tem o nome %s\n", s->num, s->name);
	
	munmap(s, sizeof(student));
	close(fd);
	shm_unlink("/mod3ex01");
	
	return 0;
}
