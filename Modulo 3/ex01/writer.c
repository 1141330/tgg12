#include "student.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

int main(void) {
	//criar zona de memoria.
	int fd = shm_open("/mod3ex01", O_CREAT | O_RDWR | O_TRUNC, S_IRUSR | S_IWUSR);
	if(fd == -1) {
		perror("Erro a criar a memoria partilhada.");
		exit(-1);
	}
	ftruncate(fd, sizeof(student)); //alocar o tamanho da estrutura.
	student *s = (student*)mmap(NULL, sizeof(student), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	
	//escrever na memoria
	s->num = 15;
	strcpy(s->name, "Test Student");
	
	//fechar a nossa escrita na memoria.
	munmap(s, sizeof(student));
	close(fd);
	// we do not shm_unlink.
	return 0;
}
