#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <time.h>

int main(){
	int vec[1000];
	int i,j;
	time_t t;
	pid_t pid[10];
	srand((unsigned) time(&t));
	for(i=0;i<1000;i++){
		vec[i]=(1+(rand() % 1000));
	}
	int fd = shm_open("/mod3ex02", O_CREAT | O_RDWR , S_IRUSR | S_IWUSR);
	if(fd == -1) {
		perror("Erro a criar a memoria partilhada.");
		exit(-1);
	}
	ftruncate(fd, sizeof(vec)); //alocar o tamanho da estrutura.
	int *s = (int*)mmap(NULL, sizeof(vec), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	for(i=0;i<10;i++){
		int min= i*100;
		int max= (i+1)*100;
		pid[i]=fork();
		if(pid[i]==0){
			int def;
			for(j=min;j<max;j++){
				if(vec[j]>def){
					def=vec[j];
				}
			
			}
			*(s+i)=def;
			exit(0);
		}
	}
	int status;
	for(i=0;i<10;i++){
	waitpid(pid[i],&status,0);
	}
	for(i=0;i<10;i++){
	printf("Encontrou como maximo %d",*(s+i));
	}
	munmap(s,sizeof(vec));
	close(fd);
	shm_unlink("/mod3ex02");
return 0;

}
