#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>

#define SHARED_TAG "/shmEx07"
#define N_TROCAS 30
#define N_NUMEROS 10
#define VALOR_INICIAL 0
#define INCREMENTO 1
#define ESCRITA 'W'
#define LEITURA 'R'
#define NENHUM 'N'

typedef struct{
	int numeros [N_NUMEROS];
	char permissao[N_NUMEROS]; // flag de sincronismo para poder escrever/ler
	int transporte; // transporta valores do elemento anterior
}buffer;

buffer iniciarValores();

int main()
{
	// NOTA: Esta resolução faz com que a leitura/escrita seja feita elemento a elemento
	// Para ser consumido o buffer completo, a cada iteração, não seria necessário um vetor de permissões mas sim uma única flag 
	int fd, i, j;
	int data_size = sizeof(buffer);

	fd = shm_open(SHARED_TAG, O_CREAT|O_RDWR, S_IRUSR|S_IWUSR);
	int tr = ftruncate(fd, data_size);

	buffer * data;
	data = (buffer *) mmap(NULL, data_size , PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);

	*data = iniciarValores(VALOR_INICIAL);
		
	pid_t pid = fork();

	if (pid >0){
		for (i=0; i<N_TROCAS/N_NUMEROS; i++){
			for (j=0; j<N_NUMEROS; j++){			
				while ((*data).permissao[j] != ESCRITA){/*espera*/}				
				printf("Pai: a produzir elemento %d\n", i * N_NUMEROS + j + 1);
				(*data).transporte += INCREMENTO; // guarda próximo valor para transporte
				(*data).numeros[j] = (*data).transporte;
				(*data).permissao[j] = LEITURA;
			}
		}
		wait(NULL);
		munmap(data, data_size);
		close(fd);
		shm_unlink(SHARED_TAG);
	}else{		
		for (i=0; i<N_TROCAS/N_NUMEROS; i++){		
			for (j=0; j<N_NUMEROS; j++){
				while((*data).permissao[j] != LEITURA){/*espera*/}					
				printf("Filho: a consumir elemento %d ... ", i * N_NUMEROS + j + 1);
				printf ("Valor %d = %d\n", i * N_NUMEROS + j + 1, (*data).numeros[j]);				
				// Dar permissões de escrita ao próximo elemento
				if (j<N_NUMEROS-1)
				{
					(*data).permissao[j+1] = ESCRITA;					
				}else{
					(*data).permissao[0] = ESCRITA;
				}
			}			
			
		}
		printf("\n\n");
		munmap(data, data_size);
		close(fd);
	}

	return 0;

}

buffer iniciarValores()
{
	int i;
	buffer buff;
	buff.numeros[0]=VALOR_INICIAL;
	for(i=0; i<N_NUMEROS; i++){
		buff.permissao[i] = NENHUM;
	}
	buff.permissao[0] = ESCRITA;
	buff.transporte = VALOR_INICIAL;

	return buff;

}
