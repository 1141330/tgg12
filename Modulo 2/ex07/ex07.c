#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>

typedef struct{
	char mensagem[5];
	int num;
}dados;

int main (void){

	dados d;
	pid_t pid1;	
	int fd[2];
	int pid[10];
	int i=1,res;
	int status;
	strcpy (d.mensagem,"Win!");
	
	
	if(pipe(fd)==-1)
		perror("Pipe falhou.");		
	
	for(i=0;i<=10;i++){
						
		pid[i]=fork();	
	
		if (pid[i]==0){
			pid1=getpid();
			close(fd[1]);
			read(fd[0],&d,sizeof(dados));
			close(fd[0]);
			printf("Filho nr %d %s, Pid: %d\n",i, d.mensagem,pid1); 
			exit(d.num);		
		}	
	}	
	close(fd[0]);
	for(i=0; i<10;i++){
		sleep(2);
		d.num = i+1;
		write(fd[1],&d,sizeof(dados));			
			
	}
	close(fd[1]);
	for(i=0;i<=10;i++){
		waitpid(pid[i],&status,0);
		if(WIFEXITED(status))
				printf("Número da jogada do vencedor %d\n",WEXITSTATUS(status));
	}
	
	
	
	return 0;
}
