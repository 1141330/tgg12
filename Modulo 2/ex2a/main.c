#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
int main (){
	int read_msg[80];
	int valorlido;
	int fd[2];
	char nomelido[80];
	pid_t pid;
	
	if(pipe(fd) == -1){
		perror("Pipe failed");
		return 1;
	}
	
	printf("Escreva o numero que deseja até 100?");
	scanf("%d",&valorlido);
	printf("Escreva a palavra que deseja?");
	scanf("%s",nomelido);
	pid=fork();
	if(pid>0){
		close(fd[0]);
		write(fd[1],&valorlido,sizeof(int));
		write(fd[1],nomelido,(strlen(nomelido)+1));
		close(fd[1]);
	}else{
		close(fd[1]);
		read(fd[0],&valorlido,sizeof(int));
		read(fd[0],nomelido,80);
		printf("Tem o valor inteiro de %d, e tem a palavra %s",valorlido,nomelido);
	
}
return 0;
}
