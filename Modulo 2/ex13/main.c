#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>


#define BUFFER_SIZE 10000

int main(int argc,  char *argv[ ]) {
	pid_t pid;
	int n;
	int dados[2];
	char read_msg[BUFFER_SIZE];
	

	
	if(pipe(dados) == -1){  //cria descritores
		perror("Pipe failed");
		exit(EXIT_FAILURE);
	}
	
	pid = fork();
	if(pid == 0){
		close(dados[0]);
		dup2(dados[1],1);
		close(dados[1]);
		
		execlp("sort", "sort", "fx.txt", NULL);
		exit(1);
		
	}else{
		wait(NULL);
		close(dados[1]);
		while((n = read(dados[0],read_msg,BUFFER_SIZE))!=0){
			read_msg[n-1] = 0;
			
		}
		
		close(dados[0]);
		
	}
	
	
	
	
	return 0;
}
