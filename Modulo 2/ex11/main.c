#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

#define BUFFER 80
#define pipe_one 0
#define pipe_two 1
#define pipe_read 0
#define pipe_write 1

typedef struct{
	int cod;
	char nome[BUFFER];
	float preco;
}produto;

int main(){
	
	//pipes
	int fd[2][2];
	
	int i;
	
	int codigo = 1;
	float valor = 10.50;
	produto vec[3];
	int j = 0;
	int c;
	int value = 100;
	//produto p;
	char prod[BUFFER];
	int turn = 1;
	pid_t pids[3];
	int code;
	
	//info dos produtos
	strcpy(vec[0].nome, "produto1");
	strcpy(vec[1].nome, "produto2");
	strcpy(vec[2].nome, "produto3");
	for( i=0; i<3;i++){
		vec[i].cod = codigo;
		vec[i].preco = valor;
		codigo = codigo + i;
		valor = valor + 10;
	}


	if(pipe(fd[pipe_one])){
		perror("Pipe Failed");
	}
	if(pipe(fd[pipe_two])){
		perror("Pipe Failed");
	}
	
	for(i=0; i<3;i++){
		
		pids[i] = fork();
		
		
		if (pids[i] == 0){ //son 
			
			
			code = i + 1; //igual ao código dos produtos
			close(fd[pipe_one][pipe_read]);
			close(fd[pipe_two][pipe_write]);
			
			//requesting by code;
			if (turn == 1){
			write(fd[pipe_one][pipe_write],&code,sizeof(int));
			turn = 0;
		}else if(turn == 0){
			//then reads form pops
			read(fd[pipe_two][pipe_read],&prod,sizeof(BUFFER));
			
			printf("\nFilho tem o produto:%s",&prod);
			
			turn = 1;
			}
			
			
			close(fd[pipe_one][pipe_write]);
			close(fd[pipe_two][pipe_read]);
			
			exit(1);
			
			}}
	for(i = 0; i < 3 ; i++){
			if (pids[i] > 0){ //father 
				
			//Error
			
			close(fd[pipe_one][pipe_write]);
			close(fd[pipe_two][pipe_read]);
			
			if(turn == 1){
			read(fd[pipe_one][pipe_read],&code,sizeof(int));
		
			//encontrar produto
			for (c = 0; c < 3; c++){
				if(code == vec[c].cod){
					// its a go
				value = c; //guarda posição do vector 
				
			}
			
			}
			turn = 0;
		}else if (turn == 0){
			
			if(value != 100 ){
				write(fd[pipe_two][pipe_write],&*vec[value].nome,sizeof(BUFFER));
				
			}else{
				write(fd[pipe_two][pipe_write],"error",sizeof(BUFFER));
				
			}
			turn =1;
			}
			
			close(fd[pipe_two][pipe_write]);	
			close(fd[pipe_one][pipe_read]);	
				
			wait(NULL);
			
			

	}}
	

	
	
return 0;


}
