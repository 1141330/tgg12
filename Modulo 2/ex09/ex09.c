#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define CARTEIRA 20
#define VALOR_GANHO 10
#define VALOR_PERDIDO 5
#define NUM_PIPES 3
#define MAX 5
#define MIN 1


int gerarNumAleatorio(){
	int result = (rand()%(MAX-MIN))+MIN;
	return result;
}


int main()
{
	int saldo=CARTEIRA;
	int num_recebido_do_pai;
	int num_escolhido;
	int num_aletorio_filho;
	int num_aleatorio_gerado;
	int tenta_acertar=1;
	int termina_jogo=-1;

	int fd[NUM_PIPES][2];
	int i;
	
	//Criação Pipes
	for(i=0; i<NUM_PIPES ; i++)
	{
		if(pipe(fd[i])== -1)
		{
			perror("Criação do pipe falhou\n");
			return 1;
		}
	}
	
	printf("------JOGO-------\n");
	printf("Carteira:%d\n",saldo);
	
	pid_t p;
	p=fork();
	
	if(p>0)//Pai
	{
		
		printf("\n*******************************************\n");
		printf("Aposte em um numero de 1-5 (-1 para terminar)\n");
		scanf("%d",&num_escolhido);
		
		while(num_escolhido != -1 && saldo>0)
		{
			
			close(fd[0][0]);
				write(fd[0][1],&tenta_acertar, sizeof(int));// Decide Jogar
			close(fd[0][1]);
			
			close(fd[1][1]);
			read(fd[1][0],&num_aleatorio_gerado,sizeof(int)); // Recebe Resultado do Random
			close(fd[1][0]);
			
			if(num_escolhido == num_aleatorio_gerado)
			{
				printf("Ganhou!!\n");
				saldo=saldo+VALOR_GANHO;
				printf("Carteira:%d\n",saldo);
				
				close(fd[0][0]);
					write(fd[0][1], &saldo, sizeof(int));// informa filho
				close(fd[0][1]);
				
			}
			else
			{
				printf("Perdeu!!\n");
				saldo=saldo-VALOR_PERDIDO;
				printf("Carteira:%d\n",saldo);
				
				close(fd[0][0]);
					write(fd[0][1], &saldo, sizeof(int));//informa filho
				close(fd[0][1]);
	
			}
			
			if(saldo > 0)
			{
				printf("\n*******************************************\n");
				printf("Escolhe um numero de 1-5 (escreve -1 para terminar)\n");
				scanf("%d", &num_escolhido);
				
			}
		}
		
		close(fd[0][0]);
			write(fd[0][1],&termina_jogo, sizeof(int)); //Não decide jogar
		close(fd[0][1]);	
	}
	else
	{
		
		close(fd[0][1]);
			read(fd[0][0],&num_recebido_do_pai,sizeof(int));//Recebe Decisão
		close(fd[0][1]);
		
		while(num_recebido_do_pai == 1)
		{
			
			num_aletorio_filho=gerarNumAleatorio();//Random
			close(fd[1][0]);
			write(fd[1][1],&num_aletorio_filho,sizeof(int));//Envia para o pai
			close(fd[1][1]);
			
			
			close(fd[0][1]);
				read(fd[0][0], &saldo, sizeof(int));//Recebe saldo
			close(fd[0][0]);
			
			if(saldo == 0)
			{
				num_recebido_do_pai=0;
				exit(0);
			}
			
			close(fd[0][1]);
				read(fd[0][0],&num_recebido_do_pai,sizeof(int));
			close(fd[0][0]);
		}
		exit(1);
	}
	
	printf("Volte sempre !\n");
	return 0;
}
