    #include <ctype.h>
    #include <unistd.h>
	#include <sys/types.h>
	#include <sys/wait.h>
	#include <unistd.h>
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	
int main(){

	FILE *in;
	char stringTxt[40];
	pid_t pid;
	int fd[2];
	char c;
	
	//cria o pipe
	if(pipe(fd)== -1) //Pipe
	{
		perror("Criação do pipe falhou\n");
		return 1;
	}

	pid=fork();
	if(pid>0){
		
		//Tenta abrit o ficheiro
		in = fopen("dados.txt", "r" );
		if( in == NULL )
		{
			 printf("ERRO: não consigo abrir o ficheiro: dados.txt\n");
			 exit(1);
		}
		int i=0;
		
		while( (c = fgetc( in )) != EOF )
		{	
			stringTxt[i]=c;
			
			i++;
		}

		printf("Sou o pai e li do ficheiro de texto: %s\n",stringTxt);
		fclose( in );
		
		close(fd[0]);
		write(fd[1],stringTxt,sizeof(stringTxt)+1);
		close(fd[1]);
		wait(NULL);
		
	}else{
		char dadoslido[40];
		close(fd[1]);
		read(fd[0],dadoslido,sizeof(dadoslido)+1);
		printf("Sou o filho e li do pipe: %s\n",dadoslido);
		close(fd[0]);
	}

}
