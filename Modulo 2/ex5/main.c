#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
int main (){
	int vec1[1000];
	int vec2[1000];
	int fd[2];
	pid_t pids[5];
	int i,z;
	
	if (pipe(fd) == -1) {
		printf("erro ao criar o pipe");
		exit(-1);
	}
	
	for( z = 0; z<1000;z++){
		vec1[z]=z;
		vec2[z]=z;
	}
	int inicio;
	for( i = 0; i < 5; i++){
		pids[i] = fork();
		
		if(pids[i] == -1){
			perror("fork failed\n");
			exit(-1);
		}
		
		if (pids[i] == 0) {
			inicio = i * 200;
			break;
		}	
	}
	
	int resultado = 0;
	if (pids[i] == 0) {
		int fim = inicio + 200;
		for (i = inicio; i < fim; i++) {
			resultado += (vec1[i] + vec2[i]);
		}
		printf("Filho: %d - Resultado: %d\n", getpid(), resultado);
		close(fd[0]);
		write(fd[1], &resultado, sizeof(int));
		close(fd[1]);
		exit(0);
	} else {
		int status, valorFinal, somaFinal = 0;
		for (i = 0; i < 5; i++) {
			waitpid(pids[i], &status, 0);
			close(fd[1]);
			read(fd[0], &valorFinal, sizeof(int));
			close(fd[0]);
			somaFinal += valorFinal;
		}
		printf("Resultado completo: %d\n", somaFinal);
	}
	
	return 0;
}

