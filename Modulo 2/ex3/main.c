#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
int main (){
	int fd[2];
	char hello[80]="Hello world!";
	char bye[80]="Godbye!";
	
	pid_t pid;
	
	if(pipe(fd) == -1){
		perror("Pipe failed");
		return 1;
	}
	pid=fork();
	if(pid>0){
		
		close(fd[0]);
		write(fd[1],&hello,(strlen(hello)+1));
		sleep(2);
		write(fd[1],&bye,(strlen(bye)+1));
		close(fd[1]);
		wait(NULL);
	}else{
		char str1[80];
		char str2[80];
		close(fd[1]);
		read(fd[0],&str1,(strlen(hello)+1));
		printf("%s\n\n",str1);
		read(fd[0],&str2,(strlen(bye)+1));
		printf("%s\n\n",str2);
}
return 0;
}
