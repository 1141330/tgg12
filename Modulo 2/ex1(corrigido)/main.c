#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>

int main (){
	
	int fd[2];
	pid_t pid;
	
	if(pipe(fd) == -1){
		perror("Pipe failed");
		return 1;
	}
	
	pid=fork();
	if(pid>0) {
		pid_t reg=getpid();
		printf("Sou o pai e tenho o pid:%d",reg);
		close(fd[0]);
		write(fd[1],&reg,sizeof(int));
		close(fd[1]);
		wait(NULL);
	}else{
		pid_t pidPai;
		close(fd[1]);
		read(fd[0],&pidPai,sizeof(int));
		printf("Sou o filho e recebi o pid>%d do pai.\n",pidPai);
		close(fd[0]);
		exit(0);
	
}
return 0;
}
