#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

int main(){

	pid_t pid, pid1;
	int PaiFilho[2];
	int FilhoNeto[2];
	
	if(pipe(PaiFilho)==-1){
		perror("Pipe falhou!");	
	}
	if(pipe(FilhoNeto)==-1){
		perror("Pipe falhou!");	
	}
	
	pid = fork();
	if(pid==-1){
		perror("fork falhou!");	
	}
	if(pid==0){
		pid1=fork();
		if(pid==-1){
			perror("fork falhou!");	
		}
		if(pid==0){
			close(FilhoNeto[1]);
			dup2(FilhoNeto[0],0);
			close(FilhoNeto[0]);
			
			execlp("wc","wc", "-l",(char*)0);
			perror("exec falhou");
			 
		} else{
			close(PaiFilho[1]);
			dup2(PaiFilho[0],0);
			close(PaiFilho[0]);
			
			close(FilhoNeto[0]);
			dup2(FilhoNeto[1],1);
			close(FilhoNeto[1]);
			
			execlp("sort","sort",(char*)0);
			perror("exec falhou");
		}
		 	
	} else {
		 close(PaiFilho[0]);
         dup2(PaiFilho[1],1);
         close(PaiFilho[1]);
         execlp("ls","ls","la",(char*)0);
         perror("exec falhou");
		
	}
	
	


	return 0;
	
}	
