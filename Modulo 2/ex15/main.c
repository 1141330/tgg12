#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>


int main(){
	
	pid_t pid;
	int fd[2], filho[2], valor, numero;
	
	if(pipe(fd)==-1){
		perror("pipe falhou!");	
	}
	
	pid = fork();
	if(pid>0){
		printf("Insira um valor.\n");
		scanf("%d", &valor);
		
		close(fd[0]);
		write(fd[1], &valor,sizeof(int));
		close(fd[1]);
		
		close(filho[1]);
		read(filho[0], &numero, sizeof(int));
		close(filho[0]);
		
		printf("O factorial de %d é %d\n", valor, numero);
			
			
	} else {
		close(fd[1]);
		dup2(fd[0],0);
		close(fd[0]);
		
		close(filho[0]);
		dup2(filho[1],1);
		close(filho[1]);
	
		execlp("./factorial", "./factorial", NULL);
			
	}

	return 0;
	
}	
