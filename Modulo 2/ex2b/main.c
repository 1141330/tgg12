#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>

typedef struct{
char string[80];
int numero;
}registo;

int main (){
	int read_msg[80];
	int fd[2];
	pid_t pid;
	
	pipe(fd);
	
	if (pipe(fd[d]) == -1) {
		printf("erro ao criar o pipe");
		exit(-1);
	}
	
	
	pid=fork();
	if(pid>0) {
		registo regist;
		char nomelido[80];
		int valorlido;
		printf("Escreva o numero que deseja até 100?");
		scanf("%d",&valorlido);
		printf("Escreva a palavra que deseja?");
		scanf("%s",&nomelido);
		regist.numero = valorlido;
		strcpy(regist.string, nomelido);
		close(fd[0]);
		write(fd[1],&regist,sizeof(regist));
		close(fd[1]);
		wait(NULL);
	}else{
		registo reg;
		close(fd[1]);
		read(fd[0],&reg,sizeof(reg));
		printf("Tem o valor inteiro de %d, e tem a palavra %s\n",reg.numero,reg.string);
		close(fd[0]);
		exit(0);
	
}
return 0;
}
