#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
typedef struct {
	int codigo_cliente;
	int codigo_produto;
	int quantidade;
} registo;
int main (void){
	registo reg[50000];
	int fd[2];
	pid_t pids[10];
	int i,z
	,status;
	//Preenche os registos
	for(z=0;z<50000;z++){
		reg[z].codigo_cliente=z;
		reg[z].codigo_produto=z;
		reg[z].quantidade=z;
	}
	
	if (pipe(fd) == -1) {
		printf("erro ao criar o pipe");
		exit(-1);
	}
	
	int inicio = 0;//Cria os 10 processos necessários
	for( i = 0; i < 10; i++){
		pids[i] = fork();
		
		if(pids[i] == -1){
			perror("fork failed\n");
			exit(-1);
		}
	}
	
	int resultado = 20;
	//se e filho
	
	if (pids[i] == 0) {
		int fim = inicio + 5000;
		for (i = inicio; i < fim; i++) {
			if(reg[i].quantidade>20){//caso tenha mais de 20 vendas
				printf("Filho: %d - Quantidade: %d\n", getpid(), reg[i].quantidade);
				close(fd[0]);
				write(fd[1], &reg[i], sizeof(registo));
				lose(fd[1]);
				exit(0);
				}
			}
			inicio =+5000;
	}else{	//pai
		registo maiores[50000];
		int x;
		for (i = 0; i < 10; i++) {
			waitpid(pids[i], &status, 0);
		}
		
		close(fd[1]);
		int y = 0;
		registo regRecebido;
		while((x = read(fd[0], &regRecebido, sizeof(registo))) != 0){;
			
			maiores[y]=regRecebido;
			y++;	
		}
		close(fd[0]);
		
		for( z=0; z<sizeof(maiores);z++){
			printf("No pai Vector Maiores posicao %d valor %d",z,maiores[z]);
		}
		return 0;
	}
	
	
}
