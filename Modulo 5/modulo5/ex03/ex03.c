#include <semaphore.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h> /* Para constantes de “modo” */
#include <fcntl.h> /* Para constantes O_* */
#include <pthread.h>
#include <time.h>

#define MAX_BUFFER 1000
#define TR_NUM 5

int contas[MAX_BUFFER];
int * ap;
void fillData() {
	int i;
	srand(time(NULL));
	for(i = 0; i < MAX_BUFFER; i++) {
	
		contas[i] =  rand() % 3000; 
	
	}
}

void * thread_func(void * arg){
	
	int i = *((int*)arg);
	int f;
	
	for(f = i*200; f < (i+1)*200; f++) {
	
		*ap += contas[i];
	
	}

	pthread_exit((void*)NULL);
}
	
int main (){
	int i,v[TR_NUM];
	pthread_t tr[TR_NUM];
	ap = (int*) malloc(sizeof(int));
	fillData();
	
	for(i=0; i< TR_NUM; i++) {
		v[i] = i;
		pthread_create(&tr[i], NULL, thread_func,(void*)&v[i]);
	
	}
	
	for(i=0; i< TR_NUM; i++) {
		pthread_join(tr[i],(void*)NULL);
	
	}
	
	printf("O saldo total do banco é de %d \n",*ap);
	
	free(ap);
	
	return 0;
}
