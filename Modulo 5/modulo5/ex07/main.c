#include <semaphore.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h> /* Para constantes de “modo” */
#include <fcntl.h> /* Para constantes O_* */
#include <pthread.h>
#include <time.h>
#include <limits.h>

#define MAX_BUFFER 15
#define TR_NUM 3
#define N_LOJAS 3
#define N_PROD 5

typedef struct{
	
	int id_h;
	int id_p;
	int x;
	
}produto;

produto vec[MAX_BUFFER];
produto * vec1, * vec2, * vec3;
pthread_t tr[TR_NUM];
pthread_mutex_t mutex;
int contador1,contador2,contador3,f,resultado[2];

void fillData() {
	int i,j;
	srand(time(NULL));
	for(i = 0; i < N_LOJAS; i++) {
		for(j = 0; j < N_PROD; j++) {
		vec[(i*5)+j].id_h = i+1; 
		vec[(i*5)+j].id_p = j+1;
		vec[(i*5)+j].x =  rand() % 100; 
		}
	}
}

void * thread_soma(void * arg) {
	
	int i = *((int*)arg);
	int j;
	
	pthread_mutex_lock(&mutex);
	int somatotal =0;
	switch(i) {
			case 1:		
				
				for(j=0; j < contador1; j++) {
				
					somatotal += (vec1+j)->x;
				
				}
				
				if(somatotal < resultado[0]){
				
					resultado[0] = somatotal;
					resultado[1] = i;
				}
				 printf("soma total %d vec%d\n",somatotal,i);
				break;
			
			case 2:
				
				for(j=0; j < contador2; j++) {
				
					somatotal += (vec2+j)->x;
				
				}
				
				if(somatotal < resultado[0]){
				
					resultado[0] = somatotal;
					resultado[1] = i;
				}
				
				printf("soma total %d vec%d\n",somatotal,i); 
				break;
			case 3:	
				
				for(j=0; j < contador3; j++) {
				
					somatotal += (vec3+j)->x;
					
				}
				
				if(somatotal < resultado[0]){
				
					resultado[0] = somatotal;
					resultado[1] = i;
				}
				printf("soma total %d vec%d\n",somatotal,i); 	
				break;
			
		}
	
	pthread_mutex_unlock(&mutex);	

	pthread_exit((void*)NULL);
}	


void * thread_org(void * arg){
	
	while(f != MAX_BUFFER) {
		pthread_mutex_lock(&mutex); 
		switch(vec[f].id_h) {
			case 1:		
				*(vec1+contador1) = vec[f];
				vec1 = (produto *) realloc(vec1,sizeof(produto)*(contador1+1) + sizeof(produto));
				contador1++;
				f++;
				break;
			
			case 2:
				*(vec2+contador2) = vec[f];
				vec2 = (produto *) realloc(vec2,sizeof(produto)*(contador2+1) + sizeof(produto));
				contador2++;
				f++;
				break;
			case 3:	
				*(vec3+contador3) = vec[f];
				vec3 = (produto *) realloc(vec3,sizeof(produto)*(contador3+1) + sizeof(produto));
				contador3++;
				f++;
				break; 
			
		}
		printf("Thread %u \n", pthread_self());
		pthread_mutex_unlock(&mutex);	
	}

	pthread_exit((void*)NULL);
}
	
int main (){
	resultado[0] = 100000;
	pthread_mutex_init(&mutex, NULL);
	int i,v[TR_NUM];
	vec1 = (produto*) malloc(1*sizeof(produto));
	vec2 = (produto*) malloc(1*sizeof(produto));
	vec3 = (produto*) malloc(1*sizeof(produto));
	fillData();
	contador1 = 0,contador2=0,contador3=0;
	
	
	for(i=0; i< TR_NUM; i++) {
		v[i] = i +1;
		pthread_create(&tr[i], NULL, thread_org,(void*)&v[i]);
	}
	
	for(i=0; i< TR_NUM; i++) {
		pthread_join(tr[i],(void*)NULL);
		v[i] = i +1;
		pthread_create(&tr[i], NULL, thread_soma,(void*)&v[i]);
	}
	
	
	for(i=0; i< TR_NUM; i++) {
	
		pthread_join(tr[i],(void*)NULL);
	
	}
	
	printf("A loja mais barata é %d com um preço de %d",resultado[1],resultado[0]);
	
	printf("\n");
	
	free(vec1);
	free(vec2);
	free(vec3);

	pthread_mutex_destroy(&mutex);

	return 0;
}
