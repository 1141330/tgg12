#include <semaphore.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h> /* Para constantes de “modo” */
#include <fcntl.h> /* Para constantes O_* */
#include <pthread.h>
#include <time.h>

#define TAMANHO 51
#define CHAVES 10000
#define NR_THREADS 10

int i,v[TAMANHO],vector[CHAVES],vecthread[NR_THREADS];
pthread_mutex_t mux[TAMANHO];	

void preenche_vector(int *vector){
		time_t t;
		srand((unsigned) time(&t));
		for(i = 0; i < CHAVES; i++){
			vector[i]=(0 + ( rand() % TAMANHO));
		}
		for (i = 0; i < TAMANHO; i++)
		{
			v[i]=0;
		}
		
}

void * conta (void *arg){
		int p=*((int*)arg);
		int min = p*1000;
		int max=1000*(p+1);
		for(i = min;i <max ;i++){
			pthread_mutex_lock(&mux[vector[i]]);	
			v[vector[i]]++;
			pthread_mutex_unlock(&mux[vector[i]]);	
		}
		pthread_exit((void*)NULL);
		
}

int main(){
	pthread_t threads[NR_THREADS];

	
	preenche_vector(vector);
	for (i = 0; i < TAMANHO; i++)
	{
		
		if(pthread_mutex_init(&mux[i], NULL)!=0){
			perror("Erro na criação dos mutexs");
			exit(-1);
		}
	}
	
	

	
	for(i=0;i<NR_THREADS;i++){
		vecthread[i]=i;
		
		if(pthread_create(&threads[i], NULL, conta,(void*)&vecthread[i])!=0){
			perror("Erro na criação das threads");
			exit(-1);
		}
		printf("Todas as threads foram criadas\n");
	}
	
	
	for(i=0;i<NR_THREADS;i++){
		pthread_join(threads[i], (void*)NULL);
		printf("Todas as threads terminaram\n");
	}
	
	for (i = 0; i <TAMANHO; i++)
	{
		pthread_mutex_destroy(&mux[i]);
		if(i!=TAMANHO-1){
			printf("%d,",v[i]);
		}else{
			printf("%d\n",v[i]);
			}
		
		
	}
	
	return 0;
}
