#include <semaphore.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h> /* Para constantes de “modo” */
#include <fcntl.h> /* Para constantes O_* */
#include <pthread.h>

#define COMBOIOS 5
int cancelaAB=1,cancelaBD=1,cancelaBC=1;
pthread_mutex_t muxAB,muxBD,muxBC;
pthread_cond_t condAB,condBD,condBC;

void * comboioAD (void *arg){
		printf("AD\n");
		int numero=*((int*)arg);
		char destino='D',origem='A',linhaactual[2];
		pthread_mutex_lock (&muxAB);

		while(cancelaAB!=1){
			pthread_cond_wait(&condAB, &muxAB);
		}
	
		cancelaAB--;
	
		strcpy(linhaactual,"AB");
	
		printf("aD:Estou na linha %s e sou o comboio %d com Destion %c e origem %c\n ",linhaactual,numero,destino,origem);
		
		cancelaAB++;
		pthread_cond_signal(&condAB);
		pthread_mutex_unlock (&muxAB);
		pthread_mutex_lock (&muxBD);
		
		while(cancelaBD!=1){
			pthread_cond_wait(&condBD, &muxBD);
		}
		cancelaBD--;
		strcpy(linhaactual,"BD");
		printf("AD:Estou na linha %s e sou o comboio %d com Destion %c e origem %c \n",linhaactual,numero,destino,origem);
		
		
		cancelaBD++;
		pthread_cond_signal(&condBD);
		pthread_mutex_unlock (&muxBD);
		//printf("Hora de chegada e partida %s\n",hora());
		printf("fim\n");
		pthread_exit((void*)NULL);
}

void * comboioCA (void *arg){
		int numero=*((int*)arg);
		printf("CA\n");
		char destino='A',origem='C',linhaactual[2];
		pthread_mutex_lock (&muxBC);
		
		while(cancelaBC!=1){
			pthread_cond_wait(&condBC, &muxBC);
		}
		cancelaBC--;
		strcpy(linhaactual,"BC");
		printf("AC:Estou na linha %s e sou o comboio %d com Destion %c e origem %c \n",linhaactual,numero,destino,origem);
		
		cancelaBC++;
		pthread_cond_signal(&condBC);
		pthread_mutex_unlock (&muxBC);
		pthread_mutex_lock (&muxAB);
		while(cancelaAB!=1){
			pthread_cond_wait(&condAB, &muxAB);
		}
		
		cancelaAB--;
		strcpy(linhaactual,"AB");
		printf("AC:Estou na linha %s e sou o comboio %d com Destion %c e origem %c\n",linhaactual,numero,destino,origem);
		cancelaAB++
		;
		pthread_cond_signal(&condAB);
		pthread_mutex_unlock (&muxAB);
		printf("fim\n");
		//printf("Hora de chegada e partida %s\n",hora());
		pthread_exit((void*)NULL);
		
		
		
}
int main (){
		
		pthread_t threads[COMBOIOS];
		int i, vecthread[COMBOIOS];
		
		if(pthread_mutex_init(&muxBD, NULL)!=0){
			perror("Erro na criação do mutex");
			exit(-1);
		}
		
		if(pthread_mutex_init(&muxBC, NULL)!=0){
			perror("Erro na criação do mutex");
			exit(-1);
		}
		if(pthread_mutex_init(&muxAB, NULL)!=0){
			perror("Erro na criação do mutex");
			exit(-1);
		}
		if(pthread_cond_init(&condAB, NULL)!=0){
			perror("Erro na criação da condiçao");
			exit(-1);
		}
		if(pthread_cond_init(&condBD, NULL)!=0){
			perror("Erro na criação da condiçao");
			exit(-1);
		}
		if(pthread_cond_init(&condBC, NULL)!=0){
			perror("Erro na criação da condiçao");
			exit(-1);
		}
		
		for(i=0;i<COMBOIOS;i++){
			vecthread[i]=i;
			if(i%2 == 0){
				if(pthread_create(&threads[i], NULL, comboioAD,(void*)&vecthread[i])!=0){
					perror("Erro na criação das threads");
					exit(-1);
				}
			}else{
				printf("entrei\n");
			if(pthread_create(&threads[i], NULL, comboioCA,(void*)&vecthread[i])!=0){
					perror("Erro na criação das threads");
					exit(-1);
				}
			}
	
		}
			printf("Todas as threads foram criadas\n");
		for(i=0;i<COMBOIOS;i++){
			pthread_join(threads[i], (void*)NULL);
			printf("Todas as threads terminaram\n");
		}
		
		pthread_mutex_destroy(&muxAB);
		pthread_mutex_destroy(&muxBD);
		pthread_mutex_destroy(&muxBC);
		pthread_cond_destroy(&condAB);	
		pthread_cond_destroy(&condBC);	
		pthread_cond_destroy(&condBD);	
		return 0;
}


