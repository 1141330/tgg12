#include <semaphore.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h> /* Para constantes de “modo” */
#include <fcntl.h> /* Para constantes O_* */
#include <pthread.h>
#include <time.h>

#define CARACOIS 3
time_t inicio;
int sleep4=4,sleep5=5,sleep6=6,sleep3=CARACOIS;
int contador4=1,contador5=1,contador8=1,contador10=1;
pthread_mutex_t mux4,mux5,mux8,mux10;


void * caracol1(void *arg){
	int numero=*((int*)arg);
	unsigned long tempo=0;
	time_t fim;
	printf("Partida %d \n",numero);
	sleep(sleep5);
	printf("Cheguei ao ponto %d sendo a thread %d\n",numero+1,numero);
	sleep(sleep4);
	printf("Cheguei ao ponto 4 sendo a thread %d\n",numero);
	pthread_mutex_lock (&mux4);

		
		printf("Comecei e ate ao 5  :%d\n ",numero);
		sleep(contador4*sleep3);
		printf("Contador:  %d\n",contador4);
		contador4=contador4*2;
		printf("Contador:  %d\n",contador4);
		pthread_mutex_unlock (&mux4);
		printf("Cheguei ao ponto 5 sendo a thread %d\n",numero);
		sleep(sleep5);
		printf("Cheguei ao ponto 6 sendo a thread %d\n",numero);
		sleep(sleep6);
		printf("Cheguei ao ponto 8 sendo a thread %d\n",numero);
		pthread_mutex_lock (&mux8);

		printf("Comecei ate ao 9   :%d\n ",numero);
		sleep(contador8*sleep4);
		printf("Contador:  %d\n",contador8);
		contador8=contador8*2;
		printf("Contador:  %d\n",contador8);
		
		pthread_mutex_unlock (&mux8);
	
		printf("Cheguei ao ponto 9 sendo a thread %d\n",numero);
		pthread_mutex_lock (&mux10);

		
		printf("Comecei ate ao 10%d\n ",numero);
		sleep(contador10*sleep5);
		printf("Contador:  %d\n",contador10);
		contador10=contador10*2;
		printf("Contador:  %d\n",contador10);
		pthread_mutex_unlock (&mux10);
		
		printf("Cheguei ao ponto 10 sendo a thread %d\n",numero);
		sleep(sleep5);
		printf("Cheguei ao ponto 11 sendo a thread %d\n",numero);// deduzido pois nao aparece na tabela
		sleep(sleep3);
		printf("Cheguei ao ponto 15 sendo a thread %d\n",numero);
		fim = time(NULL);
		tempo = difftime(fim,inicio);
		printf("sendo a thread %d este e o meu tempo :: %lu\n", numero,tempo);
		pthread_exit((void*)NULL);
}
void * caracol2(void *arg){
	int numero=*((int*)arg);
	unsigned long tempo=0;
	time_t fim;
	printf("Partida %d \n",numero);
	sleep(sleep5);
	printf("Cheguei ao ponto %d sendo a thread %d\n",numero+1,numero);
	sleep(sleep4);
	printf("Cheguei ao ponto 4 sendo a thread %d\n",numero);
	pthread_mutex_lock (&mux4);

		
		printf("Comecei e ate ao 5%d\n ",numero);
		sleep(contador4*sleep3);
		printf("Contador:  %d\n",contador4);
		contador4=contador4*2;
		printf("Contador:  %d\n",contador4);
		pthread_mutex_unlock (&mux4);
		printf("Cheguei ao ponto 5 sendo a thread %d\n",numero);
		pthread_mutex_lock (&mux5);

		
		printf("Comecei e ate ao 5   : %d\n ",numero);
		sleep(contador5*sleep5);
		printf("Contador:  %d\n",contador5);
		contador5=contador5*2;
		printf("Contador:  %d\n",contador5);
		pthread_mutex_unlock (&mux5);
		printf("Cheguei ao ponto 7 sendo a thread %d\n",numero);
		sleep(sleep6);
		printf("Cheguei ao ponto 8 sendo a thread %d\n", numero);
		pthread_mutex_lock (&mux8);

		
		printf("Comecei ate ao 9   :%d\n ",numero);
		sleep(contador8*sleep4);
		printf("Contador:  %d\n",contador8);
		contador8=contador8*2;
		printf("Contador:  %d\n",contador8);
		pthread_mutex_unlock (&mux8);
		printf("Cheguei ao ponto 9 sendo a thread %d\n",numero);
		sleep(sleep5);
		printf("Cheguei ao ponto 13 sendo a thread %d\n",numero);
		sleep(sleep5);
		printf("Cheguei ao ponto 14 sendo a thread %d\n",numero);
		sleep(sleep3);
		printf("Cheguei ao ponto 15 sendo a thread %d\n",numero);
		fim = time(NULL);
		tempo = difftime(fim,inicio);
		printf("sendo a thread %d este e o meu tempo :: %lu\n", numero,tempo);
		pthread_exit((void*)NULL);
}
void * caracol3(void *arg){
	int numero=*((int*)arg);
	unsigned long tempo=0;
	time_t fim;
	printf("Partida %d \n",numero);
	sleep(sleep5);
	printf("Cheguei ao ponto %d sendo a thread %d\n",numero+1,numero);
	sleep(sleep4);
	printf("Cheguei ao ponto 4 sendo a thread %d\n",numero);
	pthread_mutex_lock (&mux4);

		printf("Comecei e ate ao 5 :  %d\n ",numero);
		
		sleep(contador4*sleep3);
		printf("Contador:  %d\n",contador4);
		contador4=contador4*2;
		printf("Contador:  %d\n",contador4);
	
		pthread_mutex_unlock (&mux4);
		printf("Cheguei ao ponto 5 sendo a thread %d\n",numero);
		pthread_mutex_lock (&mux5);

		
		printf("Comecei e ate ao 5 :  %d\n ",numero);
		sleep(contador5*sleep5);
		printf("Contador:  %d\n",contador5);
		contador5=contador5*2;
		printf("Contador:  %d\n",contador5);
		pthread_mutex_unlock (&mux5);
		printf("Cheguei ao ponto 7 sendo a thread %d\n",numero);
		sleep(sleep4);
		printf("Cheguei ao ponto 9 sendo a thread %d\n",numero);
		pthread_mutex_lock (&mux10);

		
		printf("Comecei ate ao 10 :  %d\n ",numero);
		sleep(contador10*sleep5);
		printf("Contador:  %d\n",contador10);
		contador10=contador10*2;
		printf("Contador:  %d\n",contador10);
		pthread_mutex_unlock (&mux10);
		printf("Cheguei ao ponto 10 sendo a thread %d\n",numero);
		sleep(sleep5);
		printf("Cheguei ao ponto 12 sendo a thread %d\n",numero);
		sleep(sleep3);//deduzido pois nao aprece na tabela
		printf("Cheguei ao ponto 15 sendo a thread %d\n",numero);
		fim = time(NULL);
		tempo = difftime(fim,inicio);
		printf("sendo a thread %d este e o meu tempo :: %lu\n", numero,tempo);

		pthread_exit((void*)NULL);
}


int main (){
		pthread_t threads[CARACOIS];
		int i, vecthread[CARACOIS];
		if(pthread_mutex_init(&mux4, NULL)!=0){
			perror("Erro na criação do mutex");
			exit(-1);
		}
		
		if(pthread_mutex_init(&mux5, NULL)!=0){
			perror("Erro na criação do mutex");
			exit(-1);
		}
		if(pthread_mutex_init(&mux8, NULL)!=0){
			perror("Erro na criação do mutex");
			exit(-1);
		}
		if(pthread_mutex_init(&mux10, NULL)!=0){
			perror("Erro na criação do mutex");
			exit(-1);
		}
		
		for (i = 0; i < CARACOIS; i++)
		{
			vecthread[i]=i;
		}
		
		inicio = time(NULL);
		if(pthread_create(&threads[0], NULL, caracol1,(void*)&vecthread[0])!=0){
					perror("Erro na criação das threads");
					exit(-1);
		}
		if(pthread_create(&threads[1], NULL, caracol2,(void*)&vecthread[1])!=0){
					perror("Erro na criação das threads");
					exit(-1);
		}
		if(pthread_create(&threads[2], NULL, caracol3,(void*)&vecthread[2])!=0){
					perror("Erro na criação das threads");
					exit(-1);
		}
	
			printf("Todas as threads foram criadas\n");
		for(i=0;i<CARACOIS;i++){
			pthread_join(threads[i], (void*)NULL);
			
		}
		printf("Todas as threads terminaram\n");
		pthread_mutex_destroy(&mux4);
		pthread_mutex_destroy(&mux5);
		pthread_mutex_destroy(&mux8);
		pthread_mutex_destroy(&mux10);
		
	return 0;
}
