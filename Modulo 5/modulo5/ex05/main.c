#include <semaphore.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h> /* Para constantes de “modo” */
#include <fcntl.h> /* Para constantes O_* */
#include <pthread.h>
#include <time.h>

#define MAX_BUFFER 1000
#define TR_NUM 5

int dados[MAX_BUFFER];
int * resultado;
pthread_t tr[TR_NUM];
int contador;
pthread_mutex_t mutexprint;
pthread_cond_t condprint;


void fillData() {
	int i;
	srand(time(NULL));
	for(i = 0; i < MAX_BUFFER; i++) {
	
		dados[i] =  1+i; //rand() % 3000; 
	
	}
}

void * thread_func(void * arg){
	
	int i = *((int*)arg);
	int f;
	
	for(f = i*200; f < (i+1)*200; f++) {
	
		*(resultado+f) = dados[f]*2+10;
	
	}
	
	
	pthread_mutex_lock (&mutexprint); 
	
	while (contador != i)
		pthread_cond_wait(&condprint, &mutexprint);
	
	for(f = i*200; f < (i+1)*200; f++) {
	
		printf("%d#",*(resultado+f));
	
	}
	
	contador++;
	pthread_cond_broadcast(&condprint);
	pthread_mutex_unlock (&mutexprint);

	pthread_exit((void*)NULL);
}
	
int main (){
	pthread_mutex_init(&mutexprint, NULL);
	pthread_cond_init(&condprint,NULL);
	int i,v[TR_NUM];
	resultado = (int*) malloc(MAX_BUFFER*sizeof(int));
	fillData();
	contador = 0;
	
	
	for(i=0; i< TR_NUM; i++) {
		v[i] = i;
		pthread_create(&tr[i], NULL, thread_func,(void*)&v[i]);
	
	}
	
	for(i=0; i< TR_NUM; i++) {
		pthread_join(tr[i],(void*)NULL);
	
	}
	
	printf("\n");
	free(resultado);
	pthread_mutex_destroy(&mutexprint);
	pthread_cond_destroy(&condprint);
	
	return 0;
}
